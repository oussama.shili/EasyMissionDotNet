using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class article_hashtag
    {
        public int articles_id { get; set; }
        public int hashtags_id { get; set; }
    }
}
