using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class teventMap : EntityTypeConfiguration<tevent>
    {
        public teventMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("tevent");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.capacity).HasColumnName("capacity");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.fee).HasColumnName("fee");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.typeEvent).HasColumnName("typeEvent");
            this.Property(t => t.visibility).HasColumnName("visibility");
            this.Property(t => t.organizer_id).HasColumnName("organizer_id");
        }
    }
}
