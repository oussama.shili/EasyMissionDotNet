using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class likeMap : EntityTypeConfiguration<like>
    {
        public likeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idArticle, t.idUser });

            // Properties
            this.Property(t => t.idArticle)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idUser)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.typelike)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("likes");
            this.Property(t => t.idArticle).HasColumnName("idArticle");
            this.Property(t => t.idUser).HasColumnName("idUser");
            this.Property(t => t.typelike).HasColumnName("typelike");
        }
    }
}
