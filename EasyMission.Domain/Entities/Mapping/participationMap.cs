using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class participationMap : EntityTypeConfiguration<participation>
    {
        public participationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idEvent, t.idUser });

            // Properties
            this.Property(t => t.idEvent)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idUser)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.status)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("participation");
            this.Property(t => t.idEvent).HasColumnName("idEvent");
            this.Property(t => t.idUser).HasColumnName("idUser");
            this.Property(t => t.participationtime).HasColumnName("participationtime");
            this.Property(t => t.status).HasColumnName("status");
        }
    }
}
