using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class entrepriseMap : EntityTypeConfiguration<entreprise>
    {
        public entrepriseMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.addresse)
                .HasMaxLength(255);

            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.name)
                .HasMaxLength(255);

            this.Property(t => t.phone)
                .HasMaxLength(255);

            this.Property(t => t.sector)
                .HasMaxLength(255);

            this.Property(t => t.type)
                .HasMaxLength(255);

            this.Property(t => t.webSite)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("entreprise");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.addresse).HasColumnName("addresse");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.phone).HasColumnName("phone");
            this.Property(t => t.sector).HasColumnName("sector");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.webSite).HasColumnName("webSite");
            this.Property(t => t.entrepriseFounder_id).HasColumnName("entrepriseFounder_id");
        }
    }
}
