using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class postulationMap : EntityTypeConfiguration<postulation>
    {
        public postulationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id_ann, t.id_user });

            // Properties
            this.Property(t => t.id_ann)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_user)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("postulation");
            this.Property(t => t.id_ann).HasColumnName("id_ann");
            this.Property(t => t.id_user).HasColumnName("id_user");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.id_candidate).HasColumnName("id_candidate");
        }
    }
}
