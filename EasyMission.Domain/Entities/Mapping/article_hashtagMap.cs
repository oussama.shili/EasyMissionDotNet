using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class article_hashtagMap : EntityTypeConfiguration<article_hashtag>
    {
        public article_hashtagMap()
        {
            // Primary Key
            this.HasKey(t => new { t.articles_id, t.hashtags_id });

            // Properties
            this.Property(t => t.articles_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.hashtags_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("article_hashtag");
            this.Property(t => t.articles_id).HasColumnName("articles_id");
            this.Property(t => t.hashtags_id).HasColumnName("hashtags_id");
        }
    }
}
