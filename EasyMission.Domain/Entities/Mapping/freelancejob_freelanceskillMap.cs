using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class freelancejob_freelanceskillMap : EntityTypeConfiguration<freelancejob_freelanceskill>
    {
        public freelancejob_freelanceskillMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FreelanceJob_id, t.freelanceJobSkills_id });

            // Properties
            this.Property(t => t.FreelanceJob_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.freelanceJobSkills_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("freelancejob_freelanceskill");
            this.Property(t => t.FreelanceJob_id).HasColumnName("FreelanceJob_id");
            this.Property(t => t.freelanceJobSkills_id).HasColumnName("freelanceJobSkills_id");
        }
    }
}
