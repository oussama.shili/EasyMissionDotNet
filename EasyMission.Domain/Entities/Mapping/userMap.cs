using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class userMap : EntityTypeConfiguration<user>
    {
        public userMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.role)
                .IsRequired()
                .HasMaxLength(31);

            this.Property(t => t.country)
                .HasMaxLength(255);

            this.Property(t => t.firstname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.gender)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.lastname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.mail)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.password)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.picture)
                .HasMaxLength(255);

            this.Property(t => t.secretResponse)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.username)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("user");
            this.Property(t => t.role).HasColumnName("role");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.birthdate).HasColumnName("birthdate");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.createdAt).HasColumnName("createdAt");
            this.Property(t => t.firstname).HasColumnName("firstname");
            this.Property(t => t.gender).HasColumnName("gender");
            this.Property(t => t.isActive).HasColumnName("isActive");
            this.Property(t => t.lastname).HasColumnName("lastname");
            this.Property(t => t.mail).HasColumnName("mail");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.phone).HasColumnName("phone");
            this.Property(t => t.picture).HasColumnName("picture");
            this.Property(t => t.secretResponse).HasColumnName("secretResponse");
            this.Property(t => t.username).HasColumnName("username");
            //this.Property(t => t.secretQuestion_id).HasColumnName("secretQuestion_id");
            //this.Property(t => t.secretQuestion).HasColumnName("secretQuestion");
            //this.Property(t => t.entreprise_id).HasColumnName("entreprise_id");
            //this.Property(t => t.entreprise).HasColumnName("entreprise");
        }
    }
}
