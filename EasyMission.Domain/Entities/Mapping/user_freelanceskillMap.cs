using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class user_freelanceskillMap : EntityTypeConfiguration<user_freelanceskill>
    {
        public user_freelanceskillMap()
        {
            // Primary Key
            this.HasKey(t => new { t.User_id, t.freelanceUserSkills_id });

            // Properties
            this.Property(t => t.User_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.freelanceUserSkills_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("user_freelanceskill");
            this.Property(t => t.User_id).HasColumnName("User_id");
            this.Property(t => t.freelanceUserSkills_id).HasColumnName("freelanceUserSkills_id");
        }
    }
}
