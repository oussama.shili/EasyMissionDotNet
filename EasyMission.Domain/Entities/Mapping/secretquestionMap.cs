using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class secretquestionMap : EntityTypeConfiguration<secretquestion>
    {
        public secretquestionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.question)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("secretquestion");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.question).HasColumnName("question");
        }
    }
}
