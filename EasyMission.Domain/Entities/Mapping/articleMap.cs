using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class articleMap : EntityTypeConfiguration<article>
    {
        public articleMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.body)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("article");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.body).HasColumnName("body");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.rubric_id).HasColumnName("rubric_id");
            this.Property(t => t.user_id).HasColumnName("user_id");
        }
    }
}
