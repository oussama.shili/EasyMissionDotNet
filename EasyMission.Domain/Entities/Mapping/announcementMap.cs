using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class announcementMap : EntityTypeConfiguration<announcement>
    {
        public announcementMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.category)
                .HasMaxLength(255);

            this.Property(t => t.locale)
                .HasMaxLength(255);

            this.Property(t => t.profile)
                .HasMaxLength(255);

            this.Property(t => t.task)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            this.Property(t => t.type)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("announcement");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.category).HasColumnName("category");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.locale).HasColumnName("locale");
            this.Property(t => t.postNb).HasColumnName("postNb");
            this.Property(t => t.profile).HasColumnName("profile");
            this.Property(t => t.remuneration).HasColumnName("remuneration");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.task).HasColumnName("task");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.recruiter_id).HasColumnName("recruiter_id");
        }
    }
}
