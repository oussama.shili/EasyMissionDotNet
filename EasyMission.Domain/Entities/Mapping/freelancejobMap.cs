using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class freelancejobMap : EntityTypeConfiguration<freelancejob>
    {
        public freelancejobMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.category)
                .HasMaxLength(255);

            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.freelancerLevel)
                .HasMaxLength(255);

            this.Property(t => t.projectLength)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("freelancejob");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.capacity).HasColumnName("capacity");
            this.Property(t => t.category).HasColumnName("category");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.freelancerLevel).HasColumnName("freelancerLevel");
            this.Property(t => t.freelancersNumber).HasColumnName("freelancersNumber");
            this.Property(t => t.isOpen).HasColumnName("isOpen");
            this.Property(t => t.projectHours).HasColumnName("projectHours");
            this.Property(t => t.projectLength).HasColumnName("projectLength");
            this.Property(t => t.salary).HasColumnName("salary");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.user_id).HasColumnName("user_id");
        }
    }
}
