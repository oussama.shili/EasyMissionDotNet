using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class freelanceproposalMap : EntityTypeConfiguration<freelanceproposal>
    {
        public freelanceproposalMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.coverLetter)
                .HasMaxLength(255);

            this.Property(t => t.details)
                .HasMaxLength(255);

            this.Property(t => t.state)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("freelanceproposal");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.coverLetter).HasColumnName("coverLetter");
            this.Property(t => t.details).HasColumnName("details");
            this.Property(t => t.freelancerBudget).HasColumnName("freelancerBudget");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.idFreelanceJob).HasColumnName("idFreelanceJob");
            this.Property(t => t.idUser).HasColumnName("idUser");
        }
    }
}
