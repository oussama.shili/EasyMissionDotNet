using EasyMission.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace EasyMission.Domain.Mapping
{
    public class mediumMap : EntityTypeConfiguration<medium>
    {
        public mediumMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.path)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("media");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.path).HasColumnName("path");
            this.Property(t => t.typeMedia).HasColumnName("typeMedia");
            this.Property(t => t.event_id).HasColumnName("event_id");
        }
    }
}
