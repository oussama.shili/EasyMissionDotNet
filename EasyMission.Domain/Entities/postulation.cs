using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class postulation
    {
        public int id_ann { get; set; }
        public int id_user { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public bool state { get; set; }
        public Nullable<int> id_candidate { get; set; }
    }
}
