﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Domain.Entities
{
    public class friend
    {
        [Key, Column(Order = 0)]
        public DateTime FriendshipDate { get; set; }

        [Column(Order = 1)]
        public bool acceptedRequest { get; set; }

        [Key, Column(Order = 2)]
        public int IdSender { get; set; }
        [ForeignKey("IdSender")]
        public user sender { get; set; }

        [Key, Column(Order = 3)]
        public int IdReceiver { get; set; }
        [ForeignKey("IdReceiver")]
        public user receiver { get; set; }

        
    }
}
