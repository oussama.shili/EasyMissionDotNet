using EasyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EasyMission.Domain.Entities
{
    public partial class user
    {
        public string role { get; set; }
        public int id { get; set; }
        public Nullable<System.DateTime> birthdate { get; set; }
        public string country { get; set; }
        public Nullable<System.DateTime> createdAt { get; set; }
        public string firstname { get; set; }
        public string gender { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string lastname { get; set; }
        public string mail { get; set; }
        public string password { get; set; }
        public Nullable<int> phone { get; set; }
        [DataType(DataType.ImageUrl), Display(Name = "picture")]
        public string picture { get; set; }
        public string secretResponse { get; set; }
        public string username { get; set; }
        //public Nullable<int> secretQuestion_id { get; set; }
        //public Nullable<int> secretQuestion { get; set; }
        //public Nullable<int> entreprise_id { get; set; }
        //public Nullable<int> entreprise { get; set; }

        //public ICollection<Invitation> Invitations { get; set; }
        //public ICollection<friend> friends { get; set; }
    }
}
