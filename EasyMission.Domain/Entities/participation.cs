using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class participation
    {
        public int idEvent { get; set; }
        public int idUser { get; set; }
        public Nullable<System.DateTime> participationtime { get; set; }
        public string status { get; set; }
    }
}
