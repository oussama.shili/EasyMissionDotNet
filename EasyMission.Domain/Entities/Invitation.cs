﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Domain.Entities
{
    public class Invitation
    {
        public enum Status
        {
            Ongoing,
            Accepted,
            Declined

        }
        public Invitation()
        {

        }
        public int InvitationId { get; set; }
        public string CoverLetter { get; set; }
        public string Description { get; set; }
        public Status status { get; set; }
        [Index]
        [ForeignKey("user")]
        public int userId { get; set; }
        public user user { get; set; }
    }
}
