using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class freelanceproposal
    {
        public int id { get; set; }
        public string coverLetter { get; set; }
        public string details { get; set; }
        public float freelancerBudget { get; set; }
        public string state { get; set; }
        public Nullable<int> idFreelanceJob { get; set; }
        public Nullable<int> idUser { get; set; }
    }
}
