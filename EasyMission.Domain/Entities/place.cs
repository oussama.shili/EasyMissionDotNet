using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class place
    {
        public int id { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public Nullable<int> event_id { get; set; }
    }
}
