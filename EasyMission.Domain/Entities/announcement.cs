using System;
using System.Collections.Generic;

namespace EasyMission.Domain.Entities
{
    public partial class announcement
    {
        public int id { get; set; }
        public string category { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string locale { get; set; }
        public int postNb { get; set; }
        public string profile { get; set; }
        public double remuneration { get; set; }
        public bool state { get; set; }
        public string task { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public Nullable<int> recruiter_id { get; set; }
    }
}
