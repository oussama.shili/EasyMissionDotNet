﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyMissionMVC.Controllers.EntrepriseController
{
    public class EntrepriseController : Controller
    {
        // GET: Entreprise
        public ActionResult Index()
        {
            return View();
        }

        // GET: Entreprise/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Entreprise/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Entreprise/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Entreprise/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Entreprise/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Entreprise/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Entreprise/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
