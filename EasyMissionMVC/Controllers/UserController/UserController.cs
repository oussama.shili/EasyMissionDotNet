﻿using EasyMission.Domain.Entities;
using EasyMission.Service.FriendService;
using EasyMission.Service.UserService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EasyMissionMVC.Controllers.UserController
{
    public class UserController : Controller
    {
        ServiceFriend sf = new ServiceFriend();
        ServiceUser su = new ServiceUser();

        // GET: User
        public ActionResult ListUsers()
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("http://localhost:18080");
            Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = Client.GetAsync("EasyMission-web/rest/users").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<user>>().Result;

            }
            else
            {
                ViewBag.result = "error";
            }
            return View();
        }

       

        // GET: Register
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }


        // POST: Register
        [HttpPost]
        public ActionResult Register(user usr,HttpPostedFileBase file)
        {
            user newUser = new user();

            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Content/Upload"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }

                newUser.picture = file.FileName;
            }
            
             newUser.firstname = usr.firstname;
             newUser.lastname = usr.lastname;
             newUser.mail = usr.mail;
             newUser.username = usr.username;
             newUser.password = usr.password;
             newUser.secretResponse = usr.secretResponse;
             newUser.gender = "male";
           
            

             HttpClient client = new HttpClient();
             client.BaseAddress = new Uri("http://localhost:18080");

             if (usr.role == "candidate")
             {
                 HttpResponseMessage response = client.PostAsJsonAsync<user>("EasyMission-web/rest/candidates", newUser)
                 .ContinueWith((postTask) => postTask.Result.EnsureSuccessStatusCode()).Result;
                 ViewBag.result = response;
             }
             if (usr.role == "recruiter")
             {
                 HttpResponseMessage response = client.PostAsJsonAsync<user>("EasyMission-web/rest/recruiters", newUser)
                 .ContinueWith((postTask) => postTask.Result.EnsureSuccessStatusCode()).Result;
                 ViewBag.result = response;
             }
             if (usr.role == "entreprisefounder")
             {
                 HttpResponseMessage response = client.PostAsJsonAsync<user>("EasyMission-web/rest/entrepriseFounders", newUser)
                 .ContinueWith((postTask) => postTask.Result.EnsureSuccessStatusCode()).Result;
                 ViewBag.result = response;
             }

             return RedirectToAction("ListUsers");
        }



        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            Session["id"] = null;
            Session["username"] = null;
            Session["password"] = null;
            Session["mail"] = null;
            Session["role"] = null;
            Session["firstname"] = null;
            Session["lastname"] = null;
            Session["gender"] = null;
            Session["token"] = null;
            Session["country"] = null;
            Session["birthdate"] = null;
            Session["phone"] = null;
            Session["picture"] = null;

            return View();
        }

        // GET: Logout
        public ActionResult Logout()
        {
            Session["id"] = null;
            Session["username"] = null;
            Session["password"] = null;
            Session["mail"] = null;
            Session["role"] = null;
            Session["firstname"] = null;
            Session["lastname"] = null;
            Session["gender"] = null;
            Session["token"] = null;
            Session["country"] = null;
            Session["birthdate"] = null;
            Session["phone"] = null;
            Session["picture"] = null;

            return RedirectToAction("Login");
        }

        // POST: Login
        [HttpPost]
        public ActionResult Login(user user)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:18080");
            HttpResponseMessage response = client.PostAsJsonAsync<user>("EasyMission-web/rest/authentication", user).Result;
            string statusCode= response.StatusCode.ToString();

            if (statusCode != "NotFound")
            {
                var token = response.Content.ReadAsStringAsync().Result;

                IEnumerable<user> users = su.GetAll();
                var connectedUser = new user();
                foreach (var item in users)
                {
                    if (item.username.Equals(user.username))
                    {
                        connectedUser = item;
                        Session["id"] = connectedUser.id;
                        Session["username"] = connectedUser.username;
                        Session["password"] = connectedUser.password;
                        Session["mail"] = connectedUser.mail;
                        Session["role"] = connectedUser.role;
                        Session["firstname"] = connectedUser.firstname;
                        Session["lastname"] = connectedUser.lastname;
                        Session["gender"] = connectedUser.gender;
                        Session["token"] = token;
                        Session["country"] = connectedUser.country;
                        Session["birthdate"] = connectedUser.birthdate;
                        Session["phone"] = connectedUser.phone;
                        Session["picture"] = connectedUser.picture;

                    }
                }

                if (Session["role"].Equals("administrator"))
                {
                    return RedirectToAction("HomeBack","Admin");
                }
                return RedirectToAction("ListCandidates");

            }
            return View();



        }


        // GET: ListCandidates
        public ActionResult ListCandidates()
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("http://localhost:18080");
            Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = Client.GetAsync("EasyMission-web/rest/candidates").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<user>>().Result;
               /* foreach (user candidate in ViewBag.result)
                {
                    int idCandidate = candidate.id;
                    int idConnectedUser = Int32.Parse(Session["id"].ToString());
                    List < friend > friends = (List<friend>)sf.GetAll();
                    foreach (friend fr in friends)
                    {
                        if (fr.IdSender == idConnectedUser && fr.IdReceiver == candidate.id && fr.acceptedRequest == false)
                        {

                        }
                        if (fr.IdSender == idConnectedUser && fr.IdReceiver == candidate.id && fr.acceptedRequest == true)
                        {

                        }
                    }
                }*/

            }
            else
            {
                ViewBag.result = "error";
            }
            return View();
        }

        // POST: SendFriendRequest
        [HttpPost]
        public ActionResult SendFriendRequest(user usr)
        {
            friend friendRequest = new friend();
            friendRequest.acceptedRequest = false;
            friendRequest.IdReceiver = usr.id;
            friendRequest.IdSender = int.Parse(Session["id"].ToString());
            friendRequest.FriendshipDate = DateTime.Now;

            sf.Add(friendRequest);
            sf.Commit();

            return RedirectToAction("ListFriendRequestsSent");
        }

      

        // GET: GetFriendRequests
        public ActionResult ListFriendRequestsSent()
        {
            List<user> receivers = new List<user>();
            int idConnectedUser = Int32.Parse(Session["id"].ToString());

            foreach (var item in sf.GetAll())
            {
                if ((item.IdSender == idConnectedUser) && (item.acceptedRequest == false))
                {
                    user userReceiver = su.GetById(item.IdReceiver);

                    receivers.Add(userReceiver);
                }
            }

            return View(receivers);
        }

        // GET: GetFriendRequests
        public ActionResult ListFriendRequestsReceived()
        {
            List<user> receivers = new List<user>();
            int idConnectedUser = Int32.Parse(Session["id"].ToString());

            foreach (var item in sf.GetAll())
            {
                if ((item.IdReceiver == idConnectedUser) && (item.acceptedRequest == false))
                {
                    user userSender = su.GetById(item.IdSender);

                    receivers.Add(userSender);
                }
            }

            return View(receivers);
        }

        //EDIT: AcceptFriendRequest
        public ActionResult AcceptFriendRequest(user usrToAccept)
        {
            int idConnectedUser = Int32.Parse(Session["id"].ToString());
            int idUserToAccept = usrToAccept.id;

            List<friend> friends = (List<friend>)sf.GetAll();

            foreach(friend fr in friends)
            {
                if(fr.IdReceiver==idConnectedUser && fr.IdSender == idUserToAccept)
                {
                    fr.acceptedRequest = true;
                    sf.Update(fr);
                    sf.Commit();
                }
            }
            return RedirectToAction("ListFriends");
        }



        //DELETE: RemoveFriendRequest
        public ActionResult RemoveFriendRequest(user usrToRemove)
        {
            int idConnectedUser = Int32.Parse(Session["id"].ToString());
            int idCandidateToRemove = usrToRemove.id;

            List<friend> friends = (List<friend>)sf.GetAll();

            foreach(friend fr in friends)
            {
                if((fr.IdSender==idConnectedUser && fr.IdReceiver == idCandidateToRemove) || (fr.IdSender == idCandidateToRemove && fr.IdReceiver == idConnectedUser))
                {
                    sf.Delete(fr);
                    sf.Commit();
                }
            }
            return RedirectToAction("ListFriendRequestsReceived");
        }

        //GET: ListFriends
        public ActionResult ListFriends()
        {
            int idConnectedUser = Int32.Parse(Session["id"].ToString());

            List<friend> Allfriends = (List<friend>)sf.GetAll();
            List<user> friendsList = new List<user>();

            foreach (friend fr in Allfriends)
            {
                if (fr.IdSender == idConnectedUser  && fr.acceptedRequest==true)
                {
                    user userFriend = su.GetById(fr.IdReceiver);
                    friendsList.Add(userFriend);
                }
                if (fr.IdReceiver == idConnectedUser && fr.acceptedRequest == true)
                {
                    user userFriend = su.GetById(fr.IdSender);
                    friendsList.Add(userFriend);
                }
            }
            return View(friendsList);
        }


        //DELETE: RemoveFriend
        public ActionResult RemoveFriend(user usrToRemove)
        {
            int idConnectedUser = Int32.Parse(Session["id"].ToString());
            int idCandidateToRemove = usrToRemove.id;

            List<friend> friends = (List<friend>)sf.GetAll();

            foreach (friend fr in friends)
            {
                if ((fr.IdSender == idConnectedUser && fr.IdReceiver == idCandidateToRemove) || (fr.IdSender == idCandidateToRemove && fr.IdReceiver == idConnectedUser))
                {
                    sf.Delete(fr);
                    sf.Commit();
                }
            }
            return RedirectToAction("ListFriends");
        }


    }
}
