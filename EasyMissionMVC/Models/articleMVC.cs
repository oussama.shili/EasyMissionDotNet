using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class articleMVC
    {
        public int id { get; set; }
        public string body { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string title { get; set; }
        public Nullable<int> rubric_id { get; set; }
        public Nullable<int> user_id { get; set; }
    }
}
