using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class participationMVC
    {
        public int idEvent { get; set; }
        public int idUser { get; set; }
        public Nullable<System.DateTime> participationtime { get; set; }
        public string status { get; set; }
    }
}
