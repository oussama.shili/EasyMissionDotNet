using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class teventMVC
    {
        public int id { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<int> capacity { get; set; }
        public string description { get; set; }
        public float fee { get; set; }
        public string title { get; set; }
        public Nullable<int> typeEvent { get; set; }
        public Nullable<int> visibility { get; set; }
        public Nullable<int> organizer_id { get; set; }
    }
}
