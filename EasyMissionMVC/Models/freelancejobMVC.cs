using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class freelancejobMVC
    {
        public int id { get; set; }
        public int capacity { get; set; }
        public string category { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string description { get; set; }
        public string freelancerLevel { get; set; }
        public Nullable<int> freelancersNumber { get; set; }
        public bool isOpen { get; set; }
        public Nullable<int> projectHours { get; set; }
        public string projectLength { get; set; }
        public Nullable<double> salary { get; set; }
        public string title { get; set; }
        public Nullable<int> user_id { get; set; }
    }
}
