using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class mediumMVC
    {
        public int id { get; set; }
        public string path { get; set; }
        public Nullable<int> typeMedia { get; set; }
        public Nullable<int> event_id { get; set; }
    }
}
