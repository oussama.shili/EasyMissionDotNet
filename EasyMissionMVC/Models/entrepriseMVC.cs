using System;
using System.Collections.Generic;

namespace EasyMissionMVC.Models
{
    public class entrepriseMVC
    {
        public int id { get; set; }
        public string addresse { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string sector { get; set; }
        public string type { get; set; }
        public string webSite { get; set; }
        public Nullable<int> entrepriseFounder_id { get; set; }
    }
}
