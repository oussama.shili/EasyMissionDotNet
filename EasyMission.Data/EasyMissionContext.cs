﻿

using EasyMission.Domain.Entities;
using EasyMission.Domain.Mapping;
using MySql.Data.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;

namespace EasyMission.Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EasyMissionContext : DbContext
    {
       
        public EasyMissionContext() : base("Name=easymissionContext")
        {
        }
       
        public DbSet<announcement> announcement { get; set; }
        public DbSet<article> article { get; set; }
        public DbSet<article_hashtag> article_hashtag { get; set; }
        public DbSet<entreprise> entreprise { get; set; }
        public DbSet<freelancejob> freelancejob { get; set; }
        public DbSet<freelancejob_freelanceskill> freelancejob_freelanceskill { get; set; }
        public DbSet<freelanceproposal> freelanceproposal { get; set; }
        public DbSet<freelanceskill> freelanceskill { get; set; }
        public DbSet<hashtag> hashtag { get; set; }
        public DbSet<like> likes { get; set; }
        public DbSet<medium> media { get; set; }
        public DbSet<participation> participation { get; set; }
        public DbSet<place> place { get; set; }
        public DbSet<postulation> postulation { get; set; }
        public DbSet<rubric> rubric { get; set; }
        public DbSet<secretquestion> secretquestion { get; set; }
        public DbSet<tevent> tevent { get; set; }
        public DbSet<user> user { get; set; }
        public DbSet<user_freelanceskill> user_freelanceskill { get; set; }
        public DbSet<friend> friend { get; set; }
        // public DbSet<Invitation> invitation { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new announcementMap());
            modelBuilder.Configurations.Add(new articleMap());
            modelBuilder.Configurations.Add(new article_hashtagMap());
            modelBuilder.Configurations.Add(new entrepriseMap());
            modelBuilder.Configurations.Add(new freelancejobMap());
            modelBuilder.Configurations.Add(new freelancejob_freelanceskillMap());
            modelBuilder.Configurations.Add(new freelanceproposalMap());
            modelBuilder.Configurations.Add(new freelanceskillMap());
            modelBuilder.Configurations.Add(new hashtagMap());
            modelBuilder.Configurations.Add(new likeMap());
            modelBuilder.Configurations.Add(new mediumMap());
            modelBuilder.Configurations.Add(new participationMap());
            modelBuilder.Configurations.Add(new placeMap());
            modelBuilder.Configurations.Add(new postulationMap());
            modelBuilder.Configurations.Add(new rubricMap());
            modelBuilder.Configurations.Add(new secretquestionMap());
            modelBuilder.Configurations.Add(new teventMap());
            modelBuilder.Configurations.Add(new userMap());
            modelBuilder.Configurations.Add(new user_freelanceskillMap());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            

        }

        
    }
}

