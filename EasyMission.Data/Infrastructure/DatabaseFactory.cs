﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyMission.Data;

namespace EasyMission.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private EasyMissionContext dataContext;
        public EasyMissionContext DataContext { get { return dataContext; } }
        public DatabaseFactory()
        {
            dataContext = new EasyMissionContext();
        }
        protected override void DisposeCore()
        {
            if (DataContext != null)
                DataContext.Dispose();
        }
    }


}
