namespace EasyMission.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateFriendTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.friend",
                c => new
                    {
                        FriendshipDate = c.DateTime(nullable: false, precision: 0),
                        acceptedRequest = c.Boolean(nullable: false),
                        IdSender = c.Int(nullable: false),
                        IdReceiver = c.Int(nullable: false),
                        user_id = c.Int(),
                    })
                .PrimaryKey(t => new { t.FriendshipDate, t.IdSender, t.IdReceiver })
                .ForeignKey("dbo.user", t => t.user_id)
                .ForeignKey("dbo.user", t => t.IdReceiver, cascadeDelete: true)
                .ForeignKey("dbo.user", t => t.IdSender, cascadeDelete: true)
                .Index(t => t.IdSender)
                .Index(t => t.IdReceiver)
                .Index(t => t.user_id);
            
            AddColumn("dbo.user", "secretQuestion", c => c.Int());
            AddColumn("dbo.user", "entreprise", c => c.Int());
            DropColumn("dbo.user", "secretQuestion_id");
            DropColumn("dbo.user", "entreprise_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.user", "entreprise_id", c => c.Int());
            AddColumn("dbo.user", "secretQuestion_id", c => c.Int());
            DropForeignKey("dbo.friend", "IdSender", "dbo.user");
            DropForeignKey("dbo.friend", "IdReceiver", "dbo.user");
            DropForeignKey("dbo.friend", "user_id", "dbo.user");
            DropIndex("dbo.friend", new[] { "user_id" });
            DropIndex("dbo.friend", new[] { "IdReceiver" });
            DropIndex("dbo.friend", new[] { "IdSender" });
            DropColumn("dbo.user", "entreprise");
            DropColumn("dbo.user", "secretQuestion");
            DropTable("dbo.friend");
        }
    }
}
