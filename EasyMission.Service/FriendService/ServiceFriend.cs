﻿using EasyMission.Domain.Entities;
using EasyMission.Data.Infrastructure;
using EasyMission.ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Service.FriendService
{
    public class ServiceFriend : Service<friend>, IServiceFriend
    {
        private static IDatabaseFactory DBF = new DatabaseFactory();
        private static IUnitOfWork UOW = new UnitOfWork(DBF);

        public ServiceFriend() : base(UOW)
        {
        }
    }
}
