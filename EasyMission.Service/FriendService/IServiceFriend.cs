﻿using EasyMission.Domain.Entities;
using EasyMission.ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Service.FriendService
{
    public interface IServiceFriend : IService<friend>
    {
    }
}
