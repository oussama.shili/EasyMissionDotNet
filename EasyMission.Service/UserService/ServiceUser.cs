﻿using EasyMission.Data.Infrastructure;
using EasyMission.Domain.Entities;
using EasyMission.Service.FriendService;
using EasyMission.ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Service.UserService
{
    public class ServiceUser : Service<user>, IServiceUser
    {
        private static IDatabaseFactory DBF = new DatabaseFactory();
        private static IUnitOfWork UOW = new UnitOfWork(DBF);

        public ServiceUser() : base(UOW)
        {
        }
    }
}
