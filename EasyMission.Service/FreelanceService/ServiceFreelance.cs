﻿using EasyMission.Data.Infrastructure;
using EasyMission.Domain.Entities;
using EasyMission.ServicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMission.Service.FreelanceService
{
    public class ServiceFreelance : Service<freelancejob>, IServiceFreelance
    {
        private static IDatabaseFactory DBF = new DatabaseFactory();
        private static IUnitOfWork UOW = new UnitOfWork(DBF);

        public ServiceFreelance() : base(UOW)
        {
        }
    }
}
